import React, { Suspense, lazy } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';


const Login = lazy(() => import('./Login'))
const App = lazy(() => import('./Layout/App'))
const NotFund = lazy(() => import('./pages/404/index'))

function FirstRoute() {

  return (
    <div>
      <Suspense fallback={null}>
        <Switch>
          <Route path='/app/:route' component={App} />
          
          <Route path='/login' component={Login} />
          <Route path='/404' component={NotFund} />
          <Redirect to='/404' />
        </Switch>
      </Suspense>
    </div>
  );
}

export default FirstRoute;
