import { lazy } from 'react';

export const HomePageLink = '/app/home'

export const menuData = [
  {
    title: '首页',
    key: 'home',
    link: '/app/home',
    component: lazy(() => import('../pages/Home/index')),
    subTitle: []
  }, {
    title: '主标题0',
    key: 'main0',
    link: '',
    subTitle: [
      {
        title: '副标题0',
        key: 'main0-fu0',
        link: '/app/main0-fu0',
        component: lazy(() => import('../pages/Test0/index')),
      }
    ]
  }, {
    title: '主标题1',
    key: 'main1',
    link: '/app/main2',
    subTitle: [
      {
        title: '副标题0',
        key: 'main1-fu0',
        link: '/app/main1-fu0',
        component: lazy(() => import('../pages/Test0/index')),
      }
    ]
  }, {
    title: '主标题2',
    key: 'main2',
    link: '/app/main2',
    component: lazy(() => import('../pages/Test2')),
    subTitle: [
    ]
  }, 
];