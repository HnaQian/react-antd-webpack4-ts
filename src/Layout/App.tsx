/**
 * 页面结构。
 * 用户权限管理处
 */

import React, { lazy, Suspense } from "react";
import { Layout, Menu } from 'antd';
import {
  HomeOutlined,
  FileOutlined,
} from '@ant-design/icons';
import { inject, observer } from 'mobx-react';
import TestStore from '../store/testStore';
import { Redirect, Route, Switch, Link, RouteChildrenProps } from "react-router-dom";
import { Header } from "antd/lib/layout/layout";

import './App.less'
import { menuData } from "./router";



const { Sider } = Layout;
const { SubMenu, Item } = Menu;

const NotFund = lazy(() => import('../pages/404/index'))

@inject('testStore')
@observer
class App extends React.Component<RouteChildrenProps&{testStore: TestStore}, {collapsed: boolean, selectedKey: string, openKeys: string}> {

  constructor(props: RouteChildrenProps&{testStore: TestStore}) {
    super(props);
    const pr: any = this.props.match?.params || {}
    const selectedKey = pr.route
    this.state = {
      collapsed: false,
      selectedKey,
      openKeys: selectedKey.split('-')[0]
    }
  }
  
  componentDidMount() {
    


    
  }
  onCollapse = (collapsed: boolean) => {
    console.log(collapsed);
    this.setState({ collapsed });
  };
  render() {
    const { collapsed } = this.state;
    
    return (
      <Layout style={{background: 'rgba(0,0,0,.65)'}}>
        <Header className="header">
          <img className="logo" src="" alt="logo" />
        </Header>
        <Layout className="antd-content">
          <Sider collapsed={collapsed} theme="light" >
            <Menu 
              className="noscollerBar"
              defaultSelectedKeys={[this.state.selectedKey]}
              defaultOpenKeys={[this.state.openKeys]}
              style={{height: '100%', overflow: 'auto'}}
              mode="inline">
              {
                menuData.map((v, k) => {
                  if (v.subTitle.length) {
                    return (
                      <SubMenu key={v.key} icon={<HomeOutlined />} title={v.title}>
                        {
                          v.subTitle.map(item => <Item key={item.key}><Link to={item.link}>{item.title}</Link></Item>)
                        }
                      </SubMenu>
                    )
                  } else {
                    return <Item key={v.key} icon={<FileOutlined />}><Link to={v.link}>{v.title}</Link></Item>
                  }
                })
              }
            </Menu>
          </Sider>
          <Layout className="site-layout" style={{background: '#fff', height: 'calc(100%-64px-72px)'}}>
            <Suspense fallback={null}>
              <Switch>
                {
                  menuData.map(v => {
                    if (v.subTitle.length) {
                      return v.subTitle.map(item => <Route key={item.key} path={item.link} component={item.component} />) 
                    } else {
                      return v.component ? <Route key={v.key} path={v.link} component={v.component} /> : null
                    }
                  })
                }
                <Route path='/app/404' component={NotFund} />
                <Redirect to='/app/404' />
              </Switch>
            </Suspense>
          </Layout>
        </Layout>
      </Layout>
    )
  }
}


export default App