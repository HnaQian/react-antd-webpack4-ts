import { action, makeAutoObservable, observable} from "mobx";
class TestStore {
    @observable
    title = '1212121212'

    constructor() {
        makeAutoObservable(this)
    }

    @action
    setTitle(t: string) {
        this.title = t
    }

}
export default TestStore;