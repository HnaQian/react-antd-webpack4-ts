import { action, makeAutoObservable, observable} from "mobx";
class TestStore1 {
    @observable
    title1 = '1212121212'

    constructor() {
        makeAutoObservable(this)
    }

    @action
    setTitle(t: string) {
        this.title1 = t
    }

}
export default TestStore1;