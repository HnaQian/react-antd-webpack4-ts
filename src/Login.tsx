/**
 * 用户登录
 */


import React from "react";

import { inject, observer } from 'mobx-react';
import TestStore from './store/testStore';

@inject('testStore')
@observer
class Login extends React.Component<{}&{testStore: TestStore}, {}> {

  constructor(props: {}&{testStore: TestStore}) {
    super(props);
    this.state = {
      num: 0
    }
  }
  
  componentDidMount() {
    console.log(this.props.testStore.title)
    
  }


  render() {
    return (
      <div>
        {this.props.testStore.title}
      </div>
    )
  }
}


export default Login
