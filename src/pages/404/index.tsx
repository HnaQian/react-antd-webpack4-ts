import { Button, Result } from "antd";
import React from 'react';
import { RouteComponentProps } from "react-router-dom";
import { HomePageLink } from "../../Layout/router";

const NotFund = (props: RouteComponentProps) => {
    return (
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={<Button type="primary" onClick={() => window.location.href = HomePageLink}>Back Home</Button>}
      />
    )
}

export default NotFund


// HomePageLink