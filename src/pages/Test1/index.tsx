import React from 'react';
import './index.less'
import { observer, inject } from 'mobx-react'
import { RouteComponentProps, useParams } from 'react-router-dom';
import { Button } from 'antd';
import TestStore from '../../store/testStore';
import TestStore1 from '../../store/testStore1';

const Artist = inject("testStore", "testStore1")
  (observer((props: RouteComponentProps & {testStore: TestStore, testStore1: TestStore1}) => {
    const { userId } = useParams<{ userId: string | undefined }>()
    console.log(props.testStore1, userId)
  

    return (
      <div className="artistHome">{userId}艺术家-{props.testStore.title}
        <Button onClick={() => props.testStore.setTitle('123jnb')}>{props.testStore.title}</Button>
      </div>
    )
  })
)

export default Artist