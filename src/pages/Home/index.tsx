import { Result } from 'antd';
import React from 'react';
import { SmileOutlined } from '@ant-design/icons';
const Home = () => {
    return (
      <Result
        icon={<SmileOutlined />}
        title="Hello,Welcome!"
      />
    )
}

export default Home