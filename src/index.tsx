import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import FirstRoute from './FirstRoute';
import {Provider} from "mobx-react";
import stores from './store';
import './less/reset.less';

ReactDOM.render(
  <Provider {...stores}>
    <BrowserRouter>
      <FirstRoute />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);


